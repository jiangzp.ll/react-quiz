import React from 'react';
import InputDisplayer from "./InputDisplayer";
import '../styles/GuessPanel.less'


class GuessPanel extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      guess: '',
      result: false,
      showResult: false
    };
    this.inputOnChange = this.inputOnChange.bind(this);
    this.checkResult = this.checkResult.bind(this);
  }

  render() {
    return (
      <div>
        <section className={"result"}>
          <h1>Your Result</h1>
          <InputDisplayer show={true} value={this.state.guess.charAt(0)}/>
          <InputDisplayer show={true} value={this.state.guess.charAt(1)}/>
          <InputDisplayer show={true} value={this.state.guess.charAt(2)}/>
          <InputDisplayer show={true} value={this.state.guess.charAt(3)}/>
          {this.state.showResult ? this.state.result? <p style={{color: "green"}}>Success</p > : <p style={{color: "red"}}>Failed</p>:  ""}

        </section>
        <section className={"operation"}>
          <h1>Guess Card</h1>
          <input className={"guess-input"} type="text" onChange={this.inputOnChange}/>
          <br/>
          <button onClick={this.checkResult}>Guess</button>
        </section>
      </div>
    );
  }

  inputOnChange(event) {
    this.setState({guess: event.target.value.substring(0,4)});
  }



  checkResult() {
    let guess = this.state.guess;
    this.setState({showResult: true});
    if (guess != this.props.answer) {
      this.setState({result: false});
    } else {
      this.setState({result: true});
    }
  }
}

export default GuessPanel;
