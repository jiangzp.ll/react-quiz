import React from 'react';
import InputDisplayer from "./InputDisplayer";
import '../styles/ShowResult.less'

class ShowResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
    this.showAnswer = this.showAnswer.bind(this);

  }

  render() {
    return (
      <div>
        <section>
          <InputDisplayer show={this.state.show || this.props.show} value={this.props.answer.charAt(0)}/>
          <InputDisplayer show={this.state.show || this.props.show} value={this.props.answer.charAt(1)}/>
          <InputDisplayer show={this.state.show || this.props.show} value={this.props.answer.charAt(2)}/>
          <InputDisplayer show={this.state.show || this.props.show} value={this.props.answer.charAt(3)}/>
        </section>
        <button onClick={this.showAnswer} className={"showResultButton"}>Show Result</button>
      </div>
    );
  }
  showAnswer() {
    this.setState({show: true});
  }
}

export default ShowResult;
