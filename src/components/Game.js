import React from 'react';
import GuessPanel from "./GuessPanel";
import CreateGame from "./CreateGame";
import ShowResult from "./ShowResult";
import '../styles/Game.less'

class Game extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            answer : "",
            startGame: false
        };
        this.getAnswer = this.getAnswer.bind(this);

    }

    render() {
        return (
            <div className={"gamePanel"}>
                <div className={"GuessPanel"}>
                    <GuessPanel  answer={this.state.answer}/>
                </div>
                <div className={"ControllerPanel"}>
                    <h1>New Card</h1>
                    <CreateGame setAnswer={this.getAnswer}/>
                    <ShowResult show={this.state.startGame} answer={this.state.answer}/>
                </div>
            </div>
        )
    }

    getAnswer(answer) {
        this.setState({answer: answer, startGame: true});
        setTimeout(() => {
            this.setState({answer: answer, startGame: false});
        }, 3000);

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("new game.")
    }

}

export default Game;